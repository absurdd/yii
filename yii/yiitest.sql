-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 22 2017 г., 01:03
-- Версия сервера: 5.7.13
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yiitest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(11) NOT NULL,
  `user_role` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_role`) VALUES
(1, 'anton', 'admin'),
(2, 'andrey', 'admin'),
(3, 'sasha', 'teacher'),
(4, 'vitya', 'student'),
(5, 'egor', 'student'),
(6, 'pavel', 'teacher'),
(7, 'igor', 'student'),
(8, 'bob', 'student'),
(9, 'oleg', 'teacher'),
(10, 'vanya', 'student'),
(11, 'olya', 'student'),
(12, 'nastya', 'student'),
(13, 'yulya', 'student'),
(14, 'vasilina', 'student'),
(15, 'anna', 'student'),
(16, 'inna', 'student'),
(17, 'anna', 'student'),
(18, 'inna', 'student'),
(19, 'elena', 'student'),
(20, 'olga', 'student'),
(21, 'irina', 'student'),
(22, 'victor', 'student'),
(23, 'nadya', 'student'),
(24, 'vova', 'student'),
(25, 'petr', 'student'),
(26, 'sasha', 'student'),
(27, 'katya', 'student'),
(28, 'zera', 'student'),
(29, 'alina', 'student'),
(30, 'ira', 'teacher'),
(31, 'vladimir', 'teacher'),
(32, 'sergey', 'student'),
(33, 'evgeny', 'student');

-- --------------------------------------------------------

--
-- Структура таблицы `users_search`
--

CREATE TABLE IF NOT EXISTS `users_search` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(11) NOT NULL,
  `user_role` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
