<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsersSearch extends Users
{
    public function rules()
    {
        return [
            [['user_role'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['user_role' => $this->user_role]);
        return $dataProvider;
    }
}