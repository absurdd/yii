<?php
namespace app\models;
use Yii;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$searchModel = new UsersSearch();
$dataProvider = $searchModel->search(Yii::$app->request->get());

echo GridView::widget
    ([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' =>
        [
        'user_id',
        'user_name',
            [
                'attribute'=>'user_role',
                //'filter'=>array(Users::find()),
                'filter'=>array(ArrayHelper::map($users,'user_role','user_role')),
            ],
        ]
    ]);
?>